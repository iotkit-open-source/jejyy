function getFitments(step,is_return_cash_deposit,community_id,page_size,page_num){
    var sql=`
        select
            a.id,
            a.step,
            a.is_return_cash_deposit,
            a.created_at,
            b.type,
            b.area,
            b.building,
            b.unit,
            b.number
        from ejyy_fitment a left join ejyy_building_info b on a.building_id=b.id
        where
            a.community_id=:community_id
            ${step!=-1?"and step=:step":""}
            ${is_return_cash_deposit!=-1?"and is_return_cash_deposit=:is_return_cash_deposit":""}
        limit ${(page_num-1)*page_size},${page_size}
        
    `;
    return sql;
}

function getFitmentsCount(step,is_return_cash_deposit,community_id){
    var sql=`
        select
           count(*)
        from ejyy_fitment a left join ejyy_building_info b on a.building_id=b.id
        where
            a.community_id=:community_id
            ${step!=-1?"and step=:step":""}
            ${is_return_cash_deposit!=-1?"and is_return_cash_deposit=:is_return_cash_deposit":""}
    `;
    return sql;
}

function getUnfinishedFitmentCount(community_id,building_id){
    return `
        select count(*) from ejyy_fitment
        where community_id=:community_id and
                building_id=:building_id and
                finished_at is null
    `;
}

function getFitmentDetail(id,community_id){
    var sql=`
        select
            a.id,
            a.wechat_mp_user_id,
            a.step+0 as step,
            a.agree_user_id,
            a.agreed_at,
            a.cash_deposit,
            a.finished_at,
            a.confirm_user_id,
            a.confirmed_at,
            a.return_name,
            a.return_bank,
            a.return_bank_id,
            a.return_operate_user_id,
            a.is_return_cash_deposit,
            a.returned_at,
            a.created_at,
            b.type,
            b.area,
            b.building,
            b.unit,
            b.number,
            c.fitment_pledge,
            d.real_name
        from ejyy_fitment a left join ejyy_building_info b on a.building_id=b.id
            left join ejyy_community_setting c on a.community_id=c.community_id
            left join ejyy_wechat_mp_user d on a.wechat_mp_user_id=d.id
        where a.community_id=:community_id and a.id=:id
    `;
    return sql;
}