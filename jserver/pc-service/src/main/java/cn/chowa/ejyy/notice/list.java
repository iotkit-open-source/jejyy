package cn.chowa.ejyy.notice;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.PagedData;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.NoticeQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController("noticeList")
@RequestMapping("/pc/notice")
public class list {

    @Autowired
    private NoticeQuery noticeQuery;

    @SaCheckRole(Constants.RoleName.XQTZ)
    @VerifyCommunity(true)
    @PostMapping("/list")
    public PagedData<ObjData> getList(@RequestBody RequestData data) {
        int published = data.getInt("published", false, "^0|1$",-1);
        long communityId = data.getCommunityId();

        return PagedData.of(data.getPageNum(), data.getPageSize(),
                noticeQuery.getCommunityNotices(communityId, published,
                        data.getPageSize(), data.getPageNum())
                , noticeQuery.getCommunityNoticesCount(communityId, published));
    }

}
