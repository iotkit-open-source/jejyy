package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.PropertyCompanyUserLogin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropertyCompanyUserLoginRepository extends JpaRepository<PropertyCompanyUserLogin, Long> {
}
