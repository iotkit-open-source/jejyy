package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.BuildingInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BuildingInfoRepository extends JpaRepository<BuildingInfo, Integer> {

    long countByCommunityIdAndType(long communityId,int type);

}
