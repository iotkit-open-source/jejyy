package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ejyy_property_company_user_access_community")
public class PropertyCompanyUserAccessCommunity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("property_company_user_id")
    private long propertyCompanyUserId;

    @JsonProperty("community_id")
    private long communityId;

}
